package com.example.roomdatabase.Task2LiveData.LiveDataModel;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;


@Database(entities = {LiveDataObject.class}, exportSchema = false, version = 1)
public abstract class DataDatabase extends RoomDatabase {

    public static DataDatabase instance;
    public static final String DB_NAME = "suraaj_db";

    public static synchronized DataDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context, DataDatabase.class, DB_NAME).fallbackToDestructiveMigration().build();
        }
        return instance;
    }

    public abstract Datadao datadao();

    }
