package com.example.roomdatabase.Task2LiveData.Adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.roomdatabase.R;
import com.example.roomdatabase.Task1RoomDataBase.Modal.Data;
import com.example.roomdatabase.Task2LiveData.LiveDataModel.LiveDataObject;

import java.util.ArrayList;
import java.util.List;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.MyViewholder> {
    public List<LiveDataObject> dataList = new ArrayList<>();
    Context context;

    public DataAdapter(Context context) {
        this.context = context;
    }

    public void setDataList(List<LiveDataObject> dataList) {
        try {
            this.dataList.clear();
            this.dataList = dataList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        notifyDataSetChanged();
    }

    @Override
    public MyViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.row_item, parent, false);
        return new MyViewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewholder holder, int position) {
        if (dataList != null) {
            LiveDataObject data = dataList.get(position);
            holder.textView.setText(data.name);
        } else {
            // Covers the case of data not being ready yet.
            holder.textView.setText("No Word");
        }
    }

    @Override
    public int getItemCount() {
        if (dataList != null)
            return dataList.size();
        else return 0;
    }


    public class MyViewholder extends RecyclerView.ViewHolder {
        TextView textView;

        public MyViewholder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.txtName);

        }
    }
}
