package com.example.roomdatabase.Task2LiveData.LiveDataModel;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;


import java.util.List;

public class DataRepository {
    private Datadao datadao;
    private LiveData<List<LiveDataObject>> mAllData;

    DataRepository(Application application) {
        com.example.roomdatabase.Task2LiveData.LiveDataModel.DataDatabase db = DataDatabase.getInstance(application);
        datadao = db.datadao();
        mAllData = datadao.getAllDatas();
    }

    LiveData<List<LiveDataObject>> getDataList() {
        return mAllData;
    }

    public void insertdata(LiveDataObject data) {
        new insertAsyncTask(datadao).execute(data);
    }

    public void deletAllData() {
        new deletAsyncTask(datadao).execute();
    }

    private static class deletAsyncTask extends AsyncTask<Void, Void, Void> {
        private Datadao mAsyncTaskDao;

        public deletAsyncTask(Datadao mAsyncTaskDao) {
            this.mAsyncTaskDao = mAsyncTaskDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mAsyncTaskDao.deletAll();
            return null;
        }
    }

    private static class insertAsyncTask extends AsyncTask<LiveDataObject, Void, Void> {

        private Datadao mAsyncTaskDao;

        insertAsyncTask(Datadao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(LiveDataObject... params) {
            mAsyncTaskDao.insertdata(params[0]);
            return null;
        }
    }
}
