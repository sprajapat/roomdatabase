package com.example.roomdatabase.Task2LiveData.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.roomdatabase.R;
import com.example.roomdatabase.Task2LiveData.Adapter.DataAdapter;
import com.example.roomdatabase.Task2LiveData.LiveDataModel.DataViewModal;
import com.example.roomdatabase.Task2LiveData.LiveDataModel.LiveDataObject;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    DataViewModal dataViewModal;
    FloatingActionButton fab, fab2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        findViewById();
        init();
    }


    private void findViewById() {
        recyclerView = findViewById(R.id.recyclerView);
        fab = findViewById(R.id.fab);
        fab2 = findViewById(R.id.fab2);
    }

    private void init() {

        final DataAdapter adapter = new DataAdapter(getApplicationContext());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        dataViewModal = ViewModelProviders.of(this).get(DataViewModal.class);

        dataViewModal.getAllDatas().observe(this, new Observer<List<LiveDataObject>>() {
            @Override
            public void onChanged(List<LiveDataObject> liveDataObjects) {
                for (int i = 0; i < liveDataObjects.size(); i++) {
                    Log.e("data", liveDataObjects.get(i).id + " " + liveDataObjects.get(i).name + " " + liveDataObjects.get(i).city);
                }
                adapter.setDataList(liveDataObjects);
            }
        });

        fab.setOnClickListener(v -> dataViewModal.insertdata(new LiveDataObject("Suraj22", "Jaipur222")));

        fab2.setOnClickListener(v -> dataViewModal.deletAllData());
    }
}
