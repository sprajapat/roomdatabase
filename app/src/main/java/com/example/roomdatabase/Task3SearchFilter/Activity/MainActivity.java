package com.example.roomdatabase.Task3SearchFilter.Activity;
/*working fine*/
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.roomdatabase.ParentActivity;
import com.example.roomdatabase.R;
import com.example.roomdatabase.Task3SearchFilter.Adapter.DataAdapter;
import com.example.roomdatabase.Task3SearchFilter.LiveDataModel.DataViewModal;
import com.example.roomdatabase.Task3SearchFilter.LiveDataModel.LiveDataObject;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class MainActivity extends ParentActivity /*implements DataSource.Factory<Integer, LiveDataObject>*/ {

    RecyclerView mRecyclerView;
    DataViewModal dataViewModal;
    ProgressBar progressBar;
    FloatingActionButton fab, fab2;
    LinearLayout llPopup;
    Button btnSubmit;
    EditText edtCity, edtName, edtSearch;
    DataAdapter adapter;
    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        findViewById();
        init();
    }


    private void findViewById() {
        mRecyclerView = findViewById(R.id.recyclerView);
        fab = findViewById(R.id.fab);
        fab2 = findViewById(R.id.fab2);
        progressBar = findViewById(R.id.progressBar);
        llPopup = findViewById(R.id.llPopup);
        btnSubmit = findViewById(R.id.btnSubmit);
        edtCity = findViewById(R.id.edtCity);
        edtName = findViewById(R.id.edtName);
        edtSearch = findViewById(R.id.edtSearch);
    }

    private void init() {
        setActivityTitle("LiveData Example");
        initMainBack();
        imgMainBack.setOnClickListener(v -> {
            if (edtSearch.getVisibility() == View.GONE) {
                finish();
                return;
            } else {
                adapter.getFilter().filter("");
                edtSearch.setVisibility(View.GONE);
            }
        });

        adapter = new DataAdapter(getApplicationContext());
        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(adapter);

        dataViewModal = ViewModelProviders.of(this).get(DataViewModal.class);

        dataViewModal.getAllDatas().observe(this, new Observer<List<LiveDataObject>>() {
            @Override
            public void onChanged(List<LiveDataObject> liveDataObjects) {
                adapter.setDataList(liveDataObjects);
            }
        });

        fab.setOnClickListener(v -> llPopup.setVisibility(View.VISIBLE));

        btnSubmit.setOnClickListener(v -> {
            dataViewModal.insertdata(new LiveDataObject(edtName.getText().toString(), edtCity.getText().toString()));
            llPopup.setVisibility(View.GONE);
        });

        fab2.setOnClickListener(v -> dataViewModal.deletAllData());

        imgSearch.setOnClickListener(v -> {
            if (edtSearch.getVisibility() == View.VISIBLE) {
                adapter.getFilter().filter(edtSearch.getText());
                edtSearch.setVisibility(View.GONE);
            } else {
                edtSearch.setVisibility(View.VISIBLE);
            }
        });


        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                adapter.getFilter().filter(edtSearch.getText());
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(edtSearch.getText());
            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(edtSearch.getText());
            }
        });
    }

 /*   @Override
    public DataSource<Integer, LiveDataObject> create() {
        return null;
    }*/
}
