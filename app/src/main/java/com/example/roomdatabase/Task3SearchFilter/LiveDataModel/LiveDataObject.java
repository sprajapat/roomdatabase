package com.example.roomdatabase.Task3SearchFilter.LiveDataModel;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "livedataObject")
public class LiveDataObject {

    @PrimaryKey(autoGenerate = true)
    public int id;
    @ColumnInfo(name = "name")
    public String name;
    @ColumnInfo(name = "city")
    public String city;

    @Ignore
    public LiveDataObject(String name, String city) {
        this.name = name;
        this.city = city;
    }

    public LiveDataObject(int id, String name, String city) {
        this.id = id;
        this.name = name;
        this.city = city;
    }
}
