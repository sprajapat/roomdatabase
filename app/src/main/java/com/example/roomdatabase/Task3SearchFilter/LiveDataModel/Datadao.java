package com.example.roomdatabase.Task3SearchFilter.LiveDataModel;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface Datadao {
    @Query("Select * from livedataObject")
    LiveData<List<LiveDataObject>> getAllDatas();

//    @Query("SELECT * FROM data WHERE id = :id")
//    public LiveDataObject getItemById(int id);

    @Insert
    void insertdata(LiveDataObject liveData);

    @Update
    void updateData(LiveDataObject liveData);

    @Delete
    void deletData(LiveDataObject liveData);

    @Query("DELETE FROM livedataObject")
    void deletAll();

}
