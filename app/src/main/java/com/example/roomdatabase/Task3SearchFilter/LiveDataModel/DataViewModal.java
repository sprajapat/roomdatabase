package com.example.roomdatabase.Task3SearchFilter.LiveDataModel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;


public class DataViewModal extends AndroidViewModel {

    private DataRepository mRepository;

    private LiveData<List<LiveDataObject>> mAllData;

    public DataViewModal(Application application) {
        super(application);
        mRepository = new DataRepository(application);
        mAllData = mRepository.getDataList();
    }

    public LiveData<List<LiveDataObject>> getAllDatas() {
        return mAllData;
    }

    public void insertdata(LiveDataObject data) {
        mRepository.insertdata(data);
    }

    public void deletAllData(){
        mRepository.deletAllData();
    }

}
