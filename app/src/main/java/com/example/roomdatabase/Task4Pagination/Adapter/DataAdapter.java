package com.example.roomdatabase.Task4Pagination.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.roomdatabase.R;
import com.example.roomdatabase.Task4Pagination.LiveDataModel.LiveDataObject;

import java.util.ArrayList;
import java.util.List;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.MyViewholder> implements Filterable {
    public List<LiveDataObject> dataList = new ArrayList<>();
    public List<LiveDataObject> dataListFiltered = new ArrayList<>();
    Context context;

    public DataAdapter(Context context) {
        this.context = context;
    }


    public void setDataList(List<LiveDataObject> dataList) {
        try {
            this.dataList.clear();
            this.dataList = dataList;
            this.dataListFiltered = dataList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        notifyDataSetChanged();
    }

    public void addItems(List<LiveDataObject> dataList) {
        int i = this.dataList.size();
        this.dataList.addAll(dataList);
        notifyItemInserted(i + 1);
    }

    @Override
    public MyViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.row_item, parent, false);
        return new MyViewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewholder holder, int position) {
        if (dataListFiltered != null) {
            LiveDataObject data = dataListFiltered.get(position);
            holder.txtName.setText(data.name);
            holder.txtCity.setText(data.city);
            holder.txtId.setText(String.valueOf(data.id));
        } else {
            // Covers the case of data not being ready yet.
            holder.txtName.setText("No Word");
        }
    }

    @Override
    public int getItemCount() {
        if (dataList != null)
            return dataListFiltered.size();
        else return 0;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString();
                if (charString.isEmpty()) {
                    dataListFiltered = dataList;
                } else {
                    List<LiveDataObject> filteredList = new ArrayList<>();
                    for (LiveDataObject row : dataList) {
                        if (row.name.toLowerCase().contains(charString.toLowerCase()) || row.city.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    dataListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = dataListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                dataListFiltered = (ArrayList<LiveDataObject>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public class MyViewholder extends RecyclerView.ViewHolder {
        TextView txtName, txtId, txtCity;

        public MyViewholder(@NonNull View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.txtName);
            txtId = itemView.findViewById(R.id.txtId);
            txtCity = itemView.findViewById(R.id.txtCity);

        }
    }
}
