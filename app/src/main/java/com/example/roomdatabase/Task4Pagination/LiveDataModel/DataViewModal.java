package com.example.roomdatabase.Task4Pagination.LiveDataModel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import java.util.List;


public class DataViewModal extends AndroidViewModel {

    private DataRepository mRepository;

    private MutableLiveData<List<LiveDataObject>> mAllData;

    public DataViewModal(Application application) {
        super(application);
        mRepository = new DataRepository(application);

    }

    public MutableLiveData<List<LiveDataObject>> getAllDatas() {
        return mRepository.getDataList();
    }


}
