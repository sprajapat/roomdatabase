package com.example.roomdatabase.Task4Pagination.LiveDataModel;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.example.roomdatabase.Task4Pagination.Activity.MainActivity;
import com.example.roomdatabase.Task4Pagination.DataClass;

import java.util.ArrayList;
import java.util.List;

public class DataRepository {
    private MainActivity mainActivity = new MainActivity();
    private List<LiveDataObject> dummyData = new ArrayList<>();
    private MutableLiveData<List<LiveDataObject>> mAllData = new MutableLiveData<>();

    DataRepository(Application application) {
    }

    MutableLiveData<List<LiveDataObject>> getDataList() {
        dummyData.addAll(mainActivity.getData());
        Log.e("TAG", String.valueOf(dummyData.size()));
        if (dummyData != null) {
            mAllData.setValue(dummyData);
        } else {
        }
        return mAllData;
    }
}
