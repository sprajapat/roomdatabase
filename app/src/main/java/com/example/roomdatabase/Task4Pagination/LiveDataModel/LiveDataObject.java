package com.example.roomdatabase.Task4Pagination.LiveDataModel;


public class LiveDataObject {

    public int id;
    public String name;
    public String city;

    public LiveDataObject(String name, String city) {
        this.name = name;
        this.city = city;
    }

    public LiveDataObject(int id, String name, String city) {
        this.id = id;
        this.name = name;
        this.city = city;
    }
}
