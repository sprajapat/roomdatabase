package com.example.roomdatabase.Task1RoomDataBase.Activity;
/*layout not better*/
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.roomdatabase.Task1RoomDataBase.Modal.Data;
import com.example.roomdatabase.Task1RoomDataBase.Modal.DataDatabase;
import com.example.roomdatabase.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    Button btnGet, btnSubmit, btnDelet, btnShowData, btnDeletAll, btnUpdate;
    EditText edtData, edtId, edtIdDelet, edtUpdateCity, edtUpdateName;
    TextView txtData;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById();
        init();
    }

    private void findViewById() {
        btnGet = findViewById(R.id.btnGet);
        btnSubmit = findViewById(R.id.btnSubmit);
        txtData = findViewById(R.id.txtData);
        edtData = findViewById(R.id.edtData);
        edtId = findViewById(R.id.edtId);
        edtUpdateName = findViewById(R.id.edtUpdateName);
        edtUpdateCity = findViewById(R.id.edtUpdateCity);
        edtIdDelet = findViewById(R.id.edtIdDelet);
        btnDelet = findViewById(R.id.btnDelet);
        btnShowData = findViewById(R.id.btnShowData);
        btnDeletAll = findViewById(R.id.btnDeletAll);
        btnUpdate = findViewById(R.id.btnUpdate);
    }

    private void init() {
        final DataDatabase dataDatabase = DataDatabase.getInstance(getApplicationContext());

        btnGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Data> data = new ArrayList<>();
                data.addAll(dataDatabase.datadao().getDataList());
                int i = Integer.parseInt(edtId.getText().toString());
                txtData.setText(data.get(i).name);
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Data data = new Data(edtData.getText().toString(), "Jaipur");
                dataDatabase.datadao().insertdata(data);
            }
        });

        btnDelet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Data> data = new ArrayList<>(), data2 = new ArrayList<>();
                data.addAll(dataDatabase.datadao().getDataList());

                int i = Integer.parseInt(edtIdDelet.getText().toString());
                Data data1 = new Data(data.get(i - 1).id, data.get(i - 1).name, data.get(i - 1).city);
                Log.e("delet item ", data.get(i - 1).id + " " + data.get(i - 1).name + " " + data.get(i - 1).city);
                dataDatabase.datadao().deletData(data1);

                data2.addAll(dataDatabase.datadao().getDataList());
                Log.e("List here is >", "items");
                for (i = 0; i < data2.size(); i++) {
                    Log.e("index", data2.get(i).id + " " + data2.get(i).name + " " + data2.get(i).city);
                }
            }
        });
        btnShowData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Data> data = new ArrayList<>();
                data.addAll(dataDatabase.datadao().getDataList());
                Log.e("List of >", "items");
                for (int i = 0; i < data.size(); i++) {
                    Log.e("index", data.get(i).id + " " + data.get(i).name + " " + data.get(i).city);
                }
            }
        });
        btnDeletAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                List<LiveDataObject> data = new ArrayList<>();
//                data.addAll(dataDatabase.datadao().getDataList());
//                dataDatabase.datadao().deletAll(data);

                dataDatabase.datadao().deletAll();

            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = Integer.parseInt(edtId.getText().toString());
                List<Data> data = new ArrayList<>();
                data.addAll(dataDatabase.datadao().getDataList());

                Data data1 = new Data(data.get(i).id, edtUpdateName.getText().toString(), edtUpdateCity.getText().toString());
                dataDatabase.datadao().updateData(data1);
            }
        });
    }

}
