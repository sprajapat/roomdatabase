package com.example.roomdatabase.Task1RoomDataBase.Modal;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Data.class}, exportSchema = false, version = 1)
public abstract class DataDatabase extends RoomDatabase {

    private static DataDatabase instance;
    private static final String DB_NAME = "suraaaj_db";

    public static synchronized DataDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context, DataDatabase.class, DB_NAME).allowMainThreadQueries().build();
        }
        return instance;
    }

    public abstract Datadao datadao();

}
