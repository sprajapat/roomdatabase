package com.example.roomdatabase.Task1RoomDataBase.Activity;
/*layout better*/
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.roomdatabase.R;
import com.example.roomdatabase.Task1RoomDataBase.Adapter.DataAdapter;
import com.example.roomdatabase.Task1RoomDataBase.Modal.Data;
import com.example.roomdatabase.Task1RoomDataBase.Modal.DataDatabase;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class Main2Activity extends AppCompatActivity {
    RecyclerView recyclerView;
    FloatingActionButton fab, fab2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        findViewById();
        init();
    }


    private void findViewById() {
        recyclerView = findViewById(R.id.recyclerView);
        fab = findViewById(R.id.fab);
        fab2 = findViewById(R.id.fab2);
    }

    private void init() {
        final DataDatabase dataDatabase = DataDatabase.getInstance(getApplicationContext());

        final DataAdapter adapter = new DataAdapter(getApplicationContext());
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        List<Data> data = new ArrayList<>();
        data.addAll(dataDatabase.datadao().getDataList());
        Log.e("List of >", "items");
        for (int i = 0; i < data.size(); i++) {
            Log.e("index", data.get(i).id + " " + data.get(i).name + " " + data.get(i).city);
        }

        adapter.setDataList(data);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Data> data = new ArrayList<>();
                data.addAll(dataDatabase.datadao().getDataList());
                Log.e("List of >", "items");
                for (int i = 0; i < data.size(); i++) {
                    Log.e("index", data.get(i).id + " " + data.get(i).name + " " + data.get(i).city);
                }
                Data data1 = new Data("suraj", "Jaipur");
                dataDatabase.datadao().insertdata(data1);
                adapter.setDataList(data);
            }
        });

        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataDatabase.datadao().deletAll();
            }
        });
    }
}
