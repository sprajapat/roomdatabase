package com.example.roomdatabase.Task1RoomDataBase.Modal;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface Datadao {
    @Query("Select * from dataTable")
    List<Data> getDataList();

    @Insert
    void insertdata(Data data);

    @Update
    void updateData(Data data);

    @Delete
    void deletData(Data data);

    @Query("DELETE FROM dataTable")
    void deletAll();

//    @Delete
//    void deletAll(List<LiveDataObject> data);
/*
    @Query("VACCUM")
    void clearIds();*/

}
