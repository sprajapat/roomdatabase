package com.example.roomdatabase.Task1RoomDataBase.Modal;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "dataTable")
public class Data {

    @PrimaryKey(autoGenerate = true)
    public int id;
    @ColumnInfo(name = "name")
    public String name;
    @ColumnInfo(name = "city")
    public String city;

    @Ignore
    public Data(String name, String city) {
        this.name = name;
        this.city = city;
    }

    public Data(int id, String name, String city) {
        this.id = id;
        this.name = name;
        this.city = city;
    }
}
