package com.example.roomdatabase;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ParentActivity extends AppCompatActivity {

    public ImageView imgSearch;
    public ImageView imgMainBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public ParentActivity getActivity() {
        return ParentActivity.this;
    }

    public void setActivityTitle(String titleText) {
        try {
            TextView txtActivityTitle = findViewById(R.id.txtTitle);
            txtActivityTitle.setText(Utils.nullSafe(titleText));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initMainBack() {
        try {
            imgMainBack = findViewById(R.id.imgLeft);
            imgSearch = findViewById(R.id.imgRight);

            imgMainBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
